# Reactive Form

Angular 4 application showing how to create a Reactive Form.

To run your builds, you now need to do the following commands:

* "npm run build" to build.
* "npm run test" to run unit tests.
* "npm start" to serve the app using webpack-dev-server.
* "npm run e2e" to run protractor.
