import { Component } from '@angular/core';
import { UserInfo } from './user-info';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Reactive Bootstrap Modal Form';

  user: UserInfo;

  constructor() {}

  onSubmitted(e) {
    this.user = e;
    console.log(e);
  }
}
