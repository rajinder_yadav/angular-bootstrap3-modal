import { Component, OnInit, Output, EventEmitter} from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { UserInfo } from '../user-info';

declare var jQuery: any;

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {

  myModal: FormGroup;
  @Output() submitted: EventEmitter<UserInfo> = new EventEmitter<UserInfo>();

  constructor(public fb: FormBuilder) {
  }

  ngOnInit() {
    this.myModal = this.fb.group({
      fullname: '',
      email: ''
    });
  }

  onSubmit(f: FormGroup) {
    this.submitted.emit(f.value);
    f.reset();
    jQuery('#exampleModal').modal('hide');
  }

}
